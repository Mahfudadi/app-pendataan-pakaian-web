/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.11-MariaDB : Database - app_pakaian
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`app_pakaian` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `app_pakaian`;

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nm_kategori` varchar(30) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`id_kategori`,`nm_kategori`) values 
(1,'Kemeja'),
(2,'Kaos'),
(3,'Polo');

/*Table structure for table `pakaian` */

DROP TABLE IF EXISTS `pakaian`;

CREATE TABLE `pakaian` (
  `id_pakaian` varchar(10) NOT NULL,
  `nm_pakaian` varchar(30) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `photos` varchar(20) DEFAULT NULL,
  `harga` varchar(50) NOT NULL,
  `stok` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_pakaian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pakaian` */

insert  into `pakaian`(`id_pakaian`,`nm_pakaian`,`id_kategori`,`photos`,`harga`,`stok`) values 
('p01','Kaos Polos Cotton Combed',2,'kaos1.jpg','56000','19'),
('p02','KAOS GLOW IN THE DARK',2,'kaos2.jpg','60000','9'),
('p03','Polo Shirt Pria 3 Warna',3,'polo.jpg','85000','10'),
('p04','Kemeja Polos Lengan Panjang',1,'kemeja.jpg','67000','15'),
('p05','Kemeja Flanel Long Slevee Casu',1,'flanel.jpg','120000','5');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
