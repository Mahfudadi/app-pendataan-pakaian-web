<?php
	include 'config.php';
    $db = new database();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Tambah Data Pakaian</title>
	<head>
	<body>
		<form action="proses-data.php?aksi=tambah" method="POST">
			<fieldset>
				<p>
					<label for="id_pakaian">Id Pakaian : </label>
					<input type="text" name="id_pakaian" placeholder="Contoh : CL01" />
				</p>
				<p>
					<label for="nm_pakaian">Nama Pakaian : </label>
					<input type="text" name="nm_pakaian" />
				</p>
				<p>			
					<label for="id_kategori">Kategori : </label>
					<?php                            
            		foreach ($db->tampil_kategori() as $data){ ?>
					<label><input type="radio" name="id_kategori" value="<?php echo $data['id_kategori'] ?>"><?php echo $data['nm_kategori'] ?></label>					
					<?php } ?>
				</p>
				<p>
					<label >Foto : </label>
					<input type="file" name="photos"/>
				</p>			
				<p>
					<label for="harga">Harga : </label>
					<input type="number" name="harga" />
				</p>
				<p>
					<label for="stok">Stok : </label>
					<input type="number" name="stok"/>
				</p>				
				<p>
					<input type="submit" name="tambah" value="Tambah" />
				</p>
			</fieldset>
		</form>
	</body>
</html>