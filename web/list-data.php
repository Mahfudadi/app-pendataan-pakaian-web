<?php include("config.php"); ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Data Pakaian</title>
	</head>
	<body>
		<header>
			<h1>Data Pakaian</h1>
		</header>
		<nav>
			<!-- <a href="form-data.php">[+] Tambah Baru</a> -->
		</nav>
		<br>
		<table border="1">
			<thead>
				<tr>
					<th>Id Pakaian</th>
					<th>Nama Pakaian</th>
					<th>Kategori</th>
					<th>Harga</th>
					<th>Stok</th>
					<th>URL</th>
					<th>Foto</th>					
					<th>Tindakan</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$sql = "SELECT p.id_pakaian, p.nm_pakaian, k.nm_kategori, 
							CONCAT('http://localhost/app-pendataan-pakaian-web/images/',photos) AS url, p.harga, p.stok
							FROM pakaian p, kategori k
							WHERE p.id_kategori = k.id_kategori";
					$query = mysqli_query($db, $sql);
					while($data = mysqli_fetch_array($query)){
						echo "<tr>";
						echo "<td>".$data['id_pakaian']."</td>";
						echo "<td>".$data['nm_pakaian']."</td>";
						echo "<td>".$data['nm_kategori']."</td>";
						echo "<td>".$data['harga']."</td>";
						echo "<td>".$data['stok']."</td>";
						echo "<td>".$data['url']."</td>";
						echo "<td><center><img width='200px' src='".$data['url']."'></center></td>";
						echo "<td>";						
						echo "<a href='hapus.php?id=".$data['id_pakaian']."'>Hapus</a>";
						echo "</td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
		<?php echo "total = ".mysqli_num_rows($query); ?>
	</body>
</html>